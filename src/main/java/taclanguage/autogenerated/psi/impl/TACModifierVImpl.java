// This is a generated file. Not intended for manual editing.
package taclanguage.autogenerated.psi.impl;

import static taclanguage.autogenerated.psi.TAC_elementTypeHolder.*;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import org.jetbrains.annotations.*;
import taclanguage.autogenerated.psi.*;

public class TACModifierVImpl extends ASTWrapperPsiElement implements TACModifierV {

  public TACModifierVImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull TACVisitor visitor) {
    visitor.visitModifierV(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof TACVisitor) accept((TACVisitor) visitor);
    else super.accept(visitor);
  }
}
