// This is a generated file. Not intended for manual editing.
package taclanguage.autogenerated.psi.impl;

import static taclanguage.autogenerated.psi.TAC_elementTypeHolder.*;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import org.jetbrains.annotations.*;
import taclanguage.autogenerated.psi.*;

public class TACInstructionBodyImpl extends ASTWrapperPsiElement implements TACInstructionBody {

  public TACInstructionBodyImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull TACVisitor visitor) {
    visitor.visitInstructionBody(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof TACVisitor) accept((TACVisitor) visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public TACInstNumber getInstNumber() {
    return findNotNullChildByClass(TACInstNumber.class);
  }

  @Override
  @NotNull
  public TACInstr getInstr() {
    return findNotNullChildByClass(TACInstr.class);
  }
}
