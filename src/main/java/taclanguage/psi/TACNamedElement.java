/*
 *  BSD 2-Clause License - see ./LICENSE for details.
 */

package taclanguage.psi;

import com.intellij.psi.PsiNameIdentifierOwner;

public interface TACNamedElement extends PsiNameIdentifierOwner {}
