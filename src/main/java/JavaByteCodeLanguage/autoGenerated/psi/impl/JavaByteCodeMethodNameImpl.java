// This is a generated file. Not intended for manual editing.
package JavaByteCodeLanguage.autoGenerated.psi.impl;

import static JavaByteCodeLanguage.autoGenerated.psi.JavaByteCodeTypes.*;

import JavaByteCodeLanguage.autoGenerated.psi.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import java.util.List;
import org.jetbrains.annotations.*;

public class JavaByteCodeMethodNameImpl extends ASTWrapperPsiElement
    implements JavaByteCodeMethodName {

  public JavaByteCodeMethodNameImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull JavaByteCodeVisitor visitor) {
    visitor.visitMethodName(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof JavaByteCodeVisitor) accept((JavaByteCodeVisitor) visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public JavaByteCodeDefMethodName getDefMethodName() {
    return findNotNullChildByClass(JavaByteCodeDefMethodName.class);
  }

  @Override
  @NotNull
  public List<JavaByteCodeJType> getJTypeList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, JavaByteCodeJType.class);
  }

  @Override
  @NotNull
  public JavaByteCodeParams getParams() {
    return findNotNullChildByClass(JavaByteCodeParams.class);
  }
}
