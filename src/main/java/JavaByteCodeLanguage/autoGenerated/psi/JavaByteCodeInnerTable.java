// This is a generated file. Not intended for manual editing.
package JavaByteCodeLanguage.autoGenerated.psi;

import com.intellij.psi.PsiElement;
import java.util.List;
import org.jetbrains.annotations.*;

public interface JavaByteCodeInnerTable extends PsiElement {

  @NotNull
  List<JavaByteCodeModifierV> getModifierVList();

  @NotNull
  List<JavaByteCodeType> getTypeList();
}
