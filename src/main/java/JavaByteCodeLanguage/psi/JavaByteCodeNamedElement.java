/*
 *  BSD 2-Clause License - see ./LICENSE for details.
 */

package JavaByteCodeLanguage.psi;

import com.intellij.psi.PsiNameIdentifierOwner;

public interface JavaByteCodeNamedElement extends PsiNameIdentifierOwner {}
