/*
 *  BSD 2-Clause License - see ./LICENSE for details.
 */

/*
 *  BSD 2-Clause License - see ./LICENSE for details.
 */

package Actions.openclass;

import static globalData.GlobalData.BYTECODE_EDITOR_ID;

/** {@link OpenClassFileAction} */
public class OpenClassFileJBCAction extends PsiClassAction {

  private OpenClassFileJBCAction() {
    super(BYTECODE_EDITOR_ID);
  }
}
