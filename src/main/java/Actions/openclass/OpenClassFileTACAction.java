/*
 *  BSD 2-Clause License - see ./LICENSE for details.
 */

/*
 *  BSD 2-Clause License - see ./LICENSE for details.
 */

package Actions.openclass;

import static globalData.GlobalData.TAC_EDITOR_ID;

/** {@link OpenClassFileAction} */
public class OpenClassFileTACAction extends PsiClassAction {
  public OpenClassFileTACAction() {
    super(TAC_EDITOR_ID);
  }
}
